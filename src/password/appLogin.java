/*
 * Licencia MIT.
 */

package password;

/**
 *
 * @author Rubén Martínez Cabello - ad@martinezad.es
 * 
**/

import javax.swing.JOptionPane;

public class appLogin {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        int tamaño = Integer.parseInt( JOptionPane.showInputDialog("Número de contraseñas a generar:") );
        int longitud = Integer.parseInt( JOptionPane.showInputDialog("Longitud de las contraseñas:") );
        
        Password[] tblContraseñas = new Password[tamaño];
        Boolean[]  tblFortaleza   = new Boolean[tamaño];
        
        System.out.println("Generando "+ tamaño +" contraseñas de "+ longitud+" caracteres.");
        
        while ((tamaño--)>0)
        {

            tblContraseñas[tamaño] = new Password(longitud);
            tblFortaleza[tamaño] = tblContraseñas[tamaño].esFuerte();
            System.out.println(tblContraseñas[tamaño] + " " + ((tblFortaleza[tamaño])?"fuerte":"DEBIL"));
            //System.err.println("Generando la contraseña #"+tamaño+"...");
            //System.err.println(tblContraseñas[tamaño] + " [" + ((tblContraseñas[tamaño].esFuerte())?"fuerte":"DEBIL") + "]";
            //System.err.println();
        };
        
    }
    
}
