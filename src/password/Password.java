package password;

public class Password 
{
    public static String validos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    protected int    longitud;
    protected String contraseña;
    
    // Getter Robo^h^h^h^Hy setters
    public int getLongitud() { return this.longitud; }
    public int setLongitud(int l) { return (this.longitud = l); }
    public String getContraseña() { return this.contraseña; }
    
    
    public Password( int longitud )
    {
        this.longitud = longitud;
        //this.contraseña = "";
        this.generarPassword();
    }
    
    public Password()
    {    
        this(8);
    }
    
    boolean esFuerte()
    {
        int mayúsculas = this.contraseña.replaceAll("[^A-Z]", "").length(); // NOT A-Z
        int minúsculas = this.contraseña.replaceAll("[^a-z]", "").length(); // NOT a-z
        int números    = this.contraseña.replaceAll("[^0-9]", "").length(); // NOT 0-9
        System.out.println(mayúsculas + " MAYÚSCULAS");
        System.out.println(minúsculas + " MINÚSCULAS");
        System.out.println(números + " NÚMEROS");
        return (mayúsculas>2) && (minúsculas>1) && (números>5);
    }
    
    // Generar contaseñas de caracteres aleatorios válidos;
    void generarPassword()
    {
        
        this.contraseña = "";
        for (int i=0; i< this.getLongitud(); i++ )
        {
            int pos = (int)(Math.random() * validos.length());
            this.contraseña += validos.substring(pos, pos+1);
        };
        
    }
    
    public String toString() 
    {
        //System.err.println(super.toString() + " " + this.contraseña);
        return "CONTRASEÑA ["+this.contraseña+"]";
    }
    
    
}
